package com.waykichain.dex.commons.xservice.handler

import com.alibaba.fastjson.JSONObject
import com.waykichain.chain.vo.tx.DexSellLimitOrderTx
import com.waykichain.commons.util.WiccUtils
import com.waykichain.dex.base.dict.DexTransactionType
import com.waykichain.dex.base.dict.TradeRequestStatus
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.entity.domain.WiccTradeRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 *  Created by yehuan on 2019/7/11
 */

@Service
class DexSellLimitOrderTxHandler: DexTransactionHandler<DexSellLimitOrderTx>(){


    override fun getSupportedTxType(): DexTransactionType  = DexTransactionType.DEX_SELL_LIMIT_ORDER_TX

    override fun doHandle(tx: DexSellLimitOrderTx) {

        val request = WiccTradeRequest()

        request.tradeDirection = DexTransactionType.DEX_SELL_LIMIT_ORDER_TX.tradeDirection!!.code
        request.requestPriceType = DexTransactionType.DEX_SELL_LIMIT_ORDER_TX.priceType!!.code
        request.status = TradeRequestStatus.PENDING.code
        request.actualDealCoinAmount = BigDecimal.ZERO
        request.actualTradeAmount = BigDecimal.ZERO
        request.blockHeight = tx.valid_height
        request.dealCoinSymbol = tx.coin_type
        request.marketCoinSymbol = tx.asset_type
        request.tradePairCode = "${tx.asset_type}-${tx.coin_type}"
        request.amount = WiccUtils.convert(tx.asset_amount)
        request.requestorWalletAddress = tx.addr
        request.targetPrice = WiccUtils.convert(tx.price)
        request.txHash = tx.hash
        request.txHeight = tx.valid_height

        wiccTradeRequestService.save(request)
    }

    @Autowired
    lateinit var wiccTradeRequestService: WiccTradeRequestService

}

