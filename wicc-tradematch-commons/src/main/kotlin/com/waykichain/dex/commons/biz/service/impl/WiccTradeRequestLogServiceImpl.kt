package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.commons.biz.service.WiccTradeRequestLogService
import com.waykichain.dex.entity.domain.WiccTradeRequestLog
import com.waykichain.dex.repository.WiccTradeRequestLogRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class WiccTradeRequestLogServiceImpl: WiccTradeRequestLogService {

    override fun getById(id:Long): WiccTradeRequestLog? {
        return wiccTradeRequestLogRepository.findOne(id)
    }

    override fun save(wiccTradeRequestLog:WiccTradeRequestLog): WiccTradeRequestLog {
        return wiccTradeRequestLogRepository.saveAndFlush(wiccTradeRequestLog)
    }

    @Autowired lateinit var wiccTradeRequestLogRepository: WiccTradeRequestLogRepository
}
