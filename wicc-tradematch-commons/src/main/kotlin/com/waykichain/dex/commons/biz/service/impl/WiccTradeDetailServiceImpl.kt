package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.base.dict.TradeDetailStatus
import com.waykichain.dex.base.dict.TradeRequestDirection
import com.waykichain.dex.commons.biz.service.SettleResultService
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.entity.domain.QWiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTradeRequest
import com.waykichain.dex.repository.WiccTradeDetailRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.querydsl.QSort

@Service
class WiccTradeDetailServiceImpl: WiccTradeDetailService,SettleResultService {


    override fun findByTradedAtMs(eomsBeg: Long, eomsEnd: Long, tradePairCode: String): List<WiccTradeDetail> {
        val predicate = QWiccTradeDetail.wiccTradeDetail.tradedAtMs.between(eomsBeg, eomsEnd.minus(1))
                .and(QWiccTradeDetail.wiccTradeDetail.tradePairCode.eq(tradePairCode))
        return wiccTradeDetailRepository.findAll(predicate).toList()
    }

    override fun findFirst(tradePairCode: String): WiccTradeDetail? {
        val predicate = QWiccTradeDetail.wiccTradeDetail.tradePairCode.eq(tradePairCode)
        val sort = QSort(QWiccTradeDetail.wiccTradeDetail.tradedAtMs.asc())
        val pr = PageRequest(0, 1, sort)
        return wiccTradeDetailRepository.findAll(predicate, pr).firstOrNull()
    }

    override fun findByTradeRequest(req: WiccTradeRequest): List<WiccTradeDetail> {

        val predicate = if(req.tradeDirection == TradeRequestDirection.BUY.code)
            QWiccTradeDetail.wiccTradeDetail.buyOrderTxHash.eq(req.txHash)
        else
            QWiccTradeDetail.wiccTradeDetail.sellOrderTxHash.eq(req.txHash)

        return wiccTradeDetailRepository.findAll(predicate).toList()
    }

    override fun findByTradeId(tradeId: Long): List<WiccTradeDetail> {

        return wiccTradeDetailRepository.findAll(QWiccTradeDetail.wiccTradeDetail.tradeOrderId.eq(tradeId)).toList()
    }

    override fun updateStatus(tradeId: Long, tradeDetailStatus: TradeDetailStatus) {
        wiccTradeDetailRepository.updateStatus(tradeId, tradeDetailStatus.code)
    }

    override fun getById(id:Long): WiccTradeDetail? {
        return wiccTradeDetailRepository.findOne(id)
    }

    override fun save(wiccTradeDetail:WiccTradeDetail): WiccTradeDetail {
        return wiccTradeDetailRepository.saveAndFlush(wiccTradeDetail)
    }

    @Autowired lateinit var wiccTradeDetailRepository: WiccTradeDetailRepository
}
