package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.base.dict.TradeStatus
import com.waykichain.dex.commons.biz.service.SettleResultService
import com.waykichain.dex.commons.biz.service.WiccTradeService
import com.waykichain.dex.entity.domain.QWiccTrade
import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.repository.WiccTradeRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class WiccTradeServiceImpl: WiccTradeService,SettleResultService {

    override fun updateStatus(wiccTrade: WiccTrade, status: TradeStatus): WiccTrade {

        wiccTrade.status = status.code
        return save(wiccTrade)
    }

    override fun findByTxHash(txHash: String): WiccTrade {
        val predicate = QWiccTrade.wiccTrade.txHash.eq(txHash)
        return wiccTradeRepository.findAll(predicate).firstOrNull()!!
    }

    override fun findByHeightRange(startHeight: Int, endHeight: Int): List<WiccTrade> {

        val predicate = QWiccTrade.wiccTrade.txHeight.between(startHeight, endHeight)
        return wiccTradeRepository.findAll(predicate).toList()

    }

    override fun getById(id:Long): WiccTrade? {
        return wiccTradeRepository.findOne(id)
    }

    override fun save(wiccTrade:WiccTrade): WiccTrade {
        return wiccTradeRepository.saveAndFlush(wiccTrade)
    }


    @Autowired lateinit var wiccTradeRepository: WiccTradeRepository
}
