package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.commons.biz.service.WiccTradePairService
import com.waykichain.dex.entity.domain.QWiccTradePair
import com.waykichain.dex.entity.domain.WiccTradePair
import com.waykichain.dex.repository.WiccTradePairRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class WiccTradePairServiceImpl: WiccTradePairService {

    override fun getById(id:Long): WiccTradePair? {
        return wiccTradePairRepository.findOne(id)
    }

    override fun save(wiccTradePair:WiccTradePair): WiccTradePair {
        return wiccTradePairRepository.saveAndFlush(wiccTradePair)
    }

    override fun find(onShelf: Int): List<WiccTradePair> {
        val predicate = QWiccTradePair.wiccTradePair.onShelf.eq(onShelf)
        return wiccTradePairRepository.findAll(predicate).toList()
    }

    @Autowired lateinit var wiccTradePairRepository: WiccTradePairRepository
}
