package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccBlock

interface WiccBlockService {
    fun getById(id:Long): WiccBlock?

    fun save(wiccBlock:WiccBlock): WiccBlock
}
