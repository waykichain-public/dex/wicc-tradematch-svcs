package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccTrade

/**
 *  Created by yehuan on 2019/7/2
 */

interface SettleResultService{


    fun  onSettleSuccess(trade: WiccTrade)

    fun  onSettleFailed(trade: WiccTrade)
}