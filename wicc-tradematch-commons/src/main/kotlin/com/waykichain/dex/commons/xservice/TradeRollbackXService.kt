package com.waykichain.dex.commons.xservice

import com.waykichain.dex.commons.biz.service.WiccDexTransactionService
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.commons.biz.service.WiccTradeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 *  Created by yehuan on 2019/6/21
 */

@Service
class TradeRollbackXService{

    fun rollback(startRollbackHeight: Int, endRollbackHeight: Int){

  /*      val list = wiccDexTransactionService.findRollbackTransaction(1, 11)
        list.forEach {

            when(it.txType){
                DexTransactionType.CREATE_REQUEST.code -> undoNewRequest(it.txHash)
                DexTransactionType.CANCEL_REQUEST.code -> undoCancelRequest(it.txHash)
                DexTransactionType.FINAL_ESTIMATE.code -> undoFinalEstimate(it.txHash)
            }
        }*/

    }

    fun undoFinalEstimate(txhash: String){
        settleXService.onTxFailed(txhash)
    }

    fun undoNewRequest(txhash: String){
        wiccTradeRequestService.undoRequest(txhash)
    }

   /* fun undoCancelRequest(txhash: String){
        val trade =  wiccTradeService.findByTxHash(txhash)
        wiccTradeService.updateStatus(trade, TradeStatus.SETTLE_FAILED)
        wiccTradeDetailService.updateStatus(trade.id, TradeDetailStatus.SETTLE_FIALED)
        settleXService.rollbackTradeDetail(trade.id)
    }*/

    @Autowired lateinit var settleXService: TradeSettleXService
    @Autowired lateinit var wiccTradeRequestService: WiccTradeRequestService
    @Autowired lateinit var wiccTradeService: WiccTradeService
    @Autowired lateinit var wiccTradeDetailService: WiccTradeDetailService
    @Autowired lateinit var wiccDexTransactionService: WiccDexTransactionService

}