package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.base.dict.TradeStatus
import com.waykichain.dex.entity.domain.WiccTrade

interface WiccTradeService: SettleResultService{


    fun getById(id:Long): WiccTrade?

    fun save(wiccTrade:WiccTrade): WiccTrade

    fun findByTxHash( txHash: String): WiccTrade

    fun updateStatus(wiccTrade: WiccTrade, status: TradeStatus): WiccTrade

    override fun onSettleSuccess(trade: WiccTrade) {
        updateStatus(trade, TradeStatus.SETTLE_SUCCESSED)
    }

    override fun onSettleFailed(trade: WiccTrade) {
        updateStatus(trade, TradeStatus.SETTLE_FAILED)
    }


    fun findByHeightRange(startHeight: Int, endHeight: Int): List<WiccTrade>

}
