package com.waykichain.dex.commons.xservice.handler

import com.alibaba.fastjson.JSONObject
import com.waykichain.chain.vo.tx.DexCancelOrderTx
import com.waykichain.dex.base.dict.DexTransactionType
import com.waykichain.dex.commons.xservice.TradeRequestXService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 *  Created by yehuan on 2019/7/11
 */

@Service
class DexCancelOrderTxHandler: DexTransactionHandler<DexCancelOrderTx>(){



    override fun getSupportedTxType(): DexTransactionType  = DexTransactionType.DEX_CANCEL_ORDER_TX

    override fun doHandle(tx: DexCancelOrderTx) {

        val orderHash = tx.order_id?:return
        tradeRequestXService.onCancelRequest(orderHash)

    }


    @Autowired lateinit var tradeRequestXService: TradeRequestXService

}

