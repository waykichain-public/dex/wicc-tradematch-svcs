package com.waykichain.dex.commons.xservice.handler

/**
 *  Created by yehuan on 2019/7/12
 */

interface SubmitDexOrderHandler{

    fun submitTx()
}