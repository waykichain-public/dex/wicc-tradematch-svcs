package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/19
 */


enum class TradeStatus(val code: Int, val needRollbackWhenRequestCancel: Boolean,val description: String){

    INITIAL(0, true, "初始化"),

    SETTLING(100, true, "结算中"),

    SETTLE_SUCCESSED(200, false, "结算成功"),

    SETTLE_FAILED(900, false,"结算失败") ;

    companion object {

/*        fun rollbackableStatuses(): List<TradeStatus>{

            return TradeStatus.values().filter { it.needRollbackWhenRequestCancel == true }
        }

        fun rollbackableCodes(): List<Int>{

            val codes = arrayListOf<Int>()
            rollbackableStatuses().forEach {
                codes.add(it.code)
            }
            return codes
        }*/

        fun getByCode(statusCode: Int): TradeStatus{
            val status = TradeStatus.values().filter { it.code == statusCode }.firstOrNull()
            status?: throw IllegalArgumentException("the statusCode you given is not a legal tradeStatusCode, because of it not in the enum list")

            return status
        }

        fun needRollback(statusCode: Int) = getByCode(statusCode).needRollbackWhenRequestCancel
    }



}