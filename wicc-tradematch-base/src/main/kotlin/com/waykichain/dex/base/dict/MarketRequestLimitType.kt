package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/22
 */

enum class MarketRequestLimitType(val code: Int, val description: String){

    LIMIT_MARKET_COIN_AMOUNT(100, "限定市场币数量"),

    LIMIT_DEAL_COIN_AMOUNT(200, "限定支付货币数量")

}