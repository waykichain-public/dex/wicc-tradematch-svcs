package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/21
 */

enum class DexTransactionType(val priceType: TradeRequestPriceType?,
                              val tradeDirection: TradeRequestDirection?,
                              val code: Int, val description: String){

    DEX_SETTLE_TX             (null,null,61, "DEX_SETTLE_TX") ,          //!< dex settle Tx
    DEX_CANCEL_ORDER_TX       (null,null,62,"DEX_CANCEL_ORDER_TX") ,    //!< dex cancel order Tx
    DEX_BUY_LIMIT_ORDER_TX    ( TradeRequestPriceType.LIMIT_PRICE,TradeRequestDirection.BUY,63,"DEX_BUY_LIMIT_ORDER_TX") ,  //!< dex buy limit price order Tx
    DEX_SELL_LIMIT_ORDER_TX   ( TradeRequestPriceType.LIMIT_PRICE, TradeRequestDirection.SELL,64,"DEX_SELL_LIMIT_ORDER_TX") ,//!< dex sell limit price order Tx
    DEX_BUY_MARKET_ORDER_TX   ( TradeRequestPriceType.MARKET_PRICE,TradeRequestDirection.BUY,65,"DEX_BUY_MARKET_ORDER_TX") , //!< dex buy mark et price order Tx
    DEX_SELL_MARKET_ORDER_TX  ( TradeRequestPriceType.MARKET_PRICE,TradeRequestDirection.SELL,66,"DEX_SELL_MARKET_ORDER_TX")   //!< dex sell market price order Tx

}