package com.waykichain.dex.base.dict

/**
 * Created by shenguanglin on 2017/11/28.
 */
enum class PeriodType(val code: String, val sec: Int) {
    ONE_MIN             ("1min",        1 * 60),
    FIVE_MIN            ("5min",        5 * 60),
    FIFTEEN_MIN         ("15min",       15 * 60),
    HALF_HOUR           ("30min",       30 * 60),
    ONE_HOUR            ("1hour",       60 * 60),
    ONE_DAY             ("1day",        24 * 60 * 60),
    ONE_WEEK            ("1week",       7 * 24 * 60 * 60),
    ONE_MON             ("1mon",        30 * 24 * 60 * 60);

    companion object {
        val map = PeriodType.values().associateBy { it.code }

        fun codes() = PeriodType.values().map { it.code }

        fun getByCode(code: String) = map[code]
    }
}