package com.waykichain.bet

import com.waykichain.dex.TradeSvc
import com.waykichain.dex.commons.xservice.TradeSettleXService
import com.waykichain.dex.commons.xservice.TradeRequestXService
import com.waykichain.dex.commons.xservice.TradeRollbackXService
import org.junit.Test
import org.junit.runner.RunWith
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 *  Created by yehuan on 2019/6/25
 */


@RunWith(SpringRunner::class)
@SpringBootTest
@MapperScan("com/waykichain/dex/commons/entity/mapper")
class DexTradeTest{

    @Test
    fun testCancelRequest(){
        tradeRequestXService.onCancelRequest("440")
    }

    @Test
    fun testTradeMatch() {
        tradeSvc.run()

    }

    @Test
    fun testChainEstimateSuccess(){
        for(i in 1..9)
        tradeSettleXService.onTxSuccess(""+i)
    }

    @Autowired
    lateinit var tradeRequestXService: TradeRequestXService
    @Autowired
    lateinit var tradeSettleXService: TradeSettleXService
    @Autowired
    lateinit var tradeSvc: TradeSvc
    @Autowired
    lateinit var tradeRollbackXServicce: TradeRollbackXService

    @Test
    fun testKsort(){
        val start = System.currentTimeMillis()
        val x = arrayListOf<Int>(20,22,11,5,6,2,2021,2,3,213,221,4,11,26,23,1120,30,2,190,302,30,1,-100,1203,2,19,21,4,423,1,13,16,32,121,43,55,0,7,124,33,9,11,3)
        println(ksort(x))
        println(System.currentTimeMillis()- start)
    }

    fun ksort( arrayList: ArrayList<Int>):ArrayList<Int>{
        if(arrayList.size <=1) return arrayList
        val small: ArrayList< Int> = arrayListOf()
        val big = arrayListOf<Int>()

        for(i in 1..arrayList.size-1){
            if(arrayList[i] < arrayList[0])
                small.add(arrayList[i])
            else
                big.add(arrayList[i])
        }
        val result = arrayListOf<Int>()
        result.addAll(ksort(small))
        result.add(arrayList[0])
        result.addAll(ksort(big))
        return result
    }
}