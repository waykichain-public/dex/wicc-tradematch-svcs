package com.waykichain.test;

import com.waykichain.dex.MatchingContext;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by yehuan on 2019/7/11
 */
public class Test {

    static Object[] array = new Object[1];

    public static void main(String...args){
        try {
            System.out.println(location(getObject()));
        }catch (Exception e){

        }

    }


    public  static Object getObject() throws Exception {

        Object o = new Object() ;
        System.out.println(location(o));
        return o ;
    }


    private static Unsafe getUnsafe() throws Exception {

        Class<?> unsafeClass = Unsafe.class;

        for (Field f : unsafeClass.getDeclaredFields()) {

            if ("theUnsafe".equals(f.getName())) {

                f.setAccessible(true);

                return (Unsafe) f.get(null);

            }

        }

        throw new IllegalAccessException("no declared field: theUnsafe");

    }

    public static long location(Object object) throws Exception {

        Unsafe unsafe = getUnsafe();


        array[0] = object ;


        long baseOffset = unsafe.arrayBaseOffset(Object[].class);

        int addressSize = unsafe.addressSize();

        long location;

        switch (addressSize) {

            case 4:

                location = unsafe.getInt(array, baseOffset);

                break;

            case 8:

                location = unsafe.getLong(array, baseOffset);

                break;

            default:

                throw new Error("unsupported address size: " + addressSize);

        }

        return (location);

    }
}
