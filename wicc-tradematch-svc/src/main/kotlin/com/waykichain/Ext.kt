package com.waykichain

import com.waykichain.commons.util.WiccUtils
import java.math.BigDecimal

/**
 *  Created by yehuan on 2019/4/17
 */

fun WiccUtils.contvertDecimalLong(de: BigDecimal?): BigDecimal{

    return (de?:0.toBigDecimal()).divide(100000000.toBigDecimal(), 8 , BigDecimal.ROUND_HALF_UP)
}

fun min(a: BigDecimal, b: BigDecimal): BigDecimal{

    if(a<b)
        return a
    return b
}
