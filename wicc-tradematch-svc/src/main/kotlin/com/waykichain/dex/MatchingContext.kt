package com.waykichain.dex

import com.waykichain.chainstarter.service.CoinHandler
import com.waykichain.coin.wicc.po.SubmitDexSettleTxPO
import com.waykichain.commons.util.WiccUtils
import com.waykichain.dex.base.GLOBAL_DECIMAL_DIGITS
import com.waykichain.dex.base.SpringContextAccessor
import com.waykichain.dex.base.dict.TradeDetailStatus
import com.waykichain.dex.base.dict.TradeStatus
import com.waykichain.dex.base.GLOBAL_MINIMUM_TRADE_UNIT
import com.waykichain.dex.base.consts.DexRedisKey
import com.waykichain.dex.base.dict.TradeRequestDirection
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.commons.biz.service.WiccTradeService
import com.waykichain.dex.commons.redis.RedisLockService
import com.waykichain.dex.config.Environment
import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTradePair
import com.waykichain.dex.entity.domain.WiccTradeRequest
import com.waykichain.dex.ext.*
import com.waykichain.min
import org.slf4j.LoggerFactory
import java.lang.RuntimeException
import java.math.BigDecimal


/**
 *  Created by yehuan on 2019/6/17
**/
open class MatchingContext private constructor() : MatchingOperation{

    companion object {
        val existTradePairCode = arrayListOf<String>()
        fun valueOf(tradePair: WiccTradePair): MatchingContext{
            if(existTradePairCode.contains(tradePair.code))
                throw RuntimeException("you can create one MatchingContext only per tradePair,you had created MatchingContext which tradePairCode is ${tradePair.code}")
            existTradePairCode.add(tradePair.code)
            val context = MatchingContext()
            context.tradePair = tradePair
            return context
        }
    }

    override fun stopMatch() {
        running = false
    }

    override fun tradePair(): WiccTradePair {
        return tradePair
    }

    override fun match() {
        running = true

        while (running){
            var locked = false
            try {

                Thread.sleep(500)
                locked =  redisLockService.lock(DexRedisKey.DEX_MATCH_LOCK_REDIS_KEY, true)

                if(locked){

                    refreshContext()

                    doMatch()

                    submitSettleTx()

                    saveMatchInfo()
                }

            }catch(e:Exception){
                logger.error("matching error:::", e)
            }
            finally {
                if(locked)
                    redisLockService.unlock(DexRedisKey.DEX_MATCH_LOCK_REDIS_KEY)

            }
        }
    }

    private fun refreshContext(){
        buyRequestList = PendingRequestList.valueOf(wiccTradeRequestService.getLastPendingBuyRequest(tradePair))
        sellRequestList = PendingRequestList.valueOf(wiccTradeRequestService.getLastPendingSellRequest(tradePair))
        wiccTradeDetailList = arrayListOf()
        totalMatchedTradeAmount = BigDecimal.ZERO
        totalMatchedMoneyAmount = BigDecimal.ZERO
        settleTxHash = null
    }

    private fun doMatch(){
        while(true){

            var result = genTradeDetail() ?: break
            wiccTradeDetailList.add(result)
            totalMatchedTradeAmount = totalMatchedTradeAmount.add(result.tradeAmount)
            totalMatchedMoneyAmount = totalMatchedMoneyAmount.add(result.tradeMoney)
        }

    }


    private fun saveMatchInfo(){

        if (wiccTradeDetailList.isNotEmpty()) {

            val trade = genAndSaveWiccTrade(settleTxHash!!)
            combineTradeAndTradeDetail(trade)
            saveDetails()
            //save requests
            saveRequests(sellRequestList!!)
            saveRequests(buyRequestList!!)
        }


    }

    private fun submitSettleTx(){

        val po = SubmitDexSettleTxPO()
        po.settlerAddress = Environment.DEX_SETTLE_TX_SUBMITTER_ADDRESS
        po.dealItems = genDealItems()
        po.fee = 10000L
        settleTxHash =  coinHandler.submitDexSettleTx(po)!!

    }

    private fun saveRequests(requestList: PendingRequestList){

        for(i in 0 .. requestList.getPosition())
            wiccTradeRequestService.save(requestList.getRequest(i))
    }


    private fun genAndSaveWiccTrade(settleTxHash: String): WiccTrade{

        var trade = WiccTrade()
        trade.txHash = settleTxHash
        trade.tradePairCode = tradePair.code
        trade.dealCoinSymbol = tradePair.dealCoinSymbol
        trade.marketCoinSymbol = tradePair.marketCoinSymbol
        trade.tradeAmount = totalMatchedTradeAmount
        trade.tradeMoney = totalMatchedMoneyAmount
        trade.tradeDetailCount = wiccTradeDetailList.size
        trade.status = TradeStatus.SETTLING.code
        trade = wiccTradeService.save(trade)
        return trade
    }

    private fun combineTradeAndTradeDetail(trade: WiccTrade ){
        wiccTradeDetailList.forEach {
            it.tradeOrderId = trade.id
            it.status = TradeDetailStatus.SETTLING.code
        }

    }

    private fun saveDetails(){
        wiccTradeDetailList.forEach { wiccTradeDetailService.save(it) }
    }

    private fun genDealItems(): ArrayList<SubmitDexSettleTxPO.DealItem>{

        val list = arrayListOf<SubmitDexSettleTxPO.DealItem>()
        wiccTradeDetailList.forEach {
            val item = SubmitDexSettleTxPO.DealItem()
            item.buy_order_txid = it.buyOrderTxHash
            item.sell_order_txid = it.sellOrderTxHash
            item.deal_asset_amount = WiccUtils.convert(it.tradeAmount)
            item.deal_coin_amount = WiccUtils.convert(it.tradeMoney)
            item.price = WiccUtils.convert(it.tradePrice)
            list.add(item)
        }

        return list

    }
    private fun genTradeDetail(): WiccTradeDetail?{

        if(wiccTradeDetailList.size >= 2) return null
        val sellRequest = sellRequestList!!.currentElement()?:return null
        val buyRequest = buyRequestList!!.currentElement()?: return null

        val wiccTradeDetail = genTradeDetail(sellRequest,buyRequest)?: return null

        sellRequest.modifyAfterMatch( wiccTradeDetail)
        buyRequest.modifyAfterMatch( wiccTradeDetail)
        return wiccTradeDetail

    }

    private fun genTradeDetail(sellRequest: WiccTradeRequest, buyRequest: WiccTradeRequest): WiccTradeDetail?{

        val keyInfo = detimineTradeDetailKeyInfo(sellRequest, buyRequest)?: return null

        val wiccTradeDetail = WiccTradeDetail()
        wiccTradeDetail.sellOrderTxHash = sellRequest.txHash
        wiccTradeDetail.buyOrderTxHash = buyRequest.txHash
        wiccTradeDetail.tradeAmount = keyInfo.tradeAmount
        wiccTradeDetail.tradePairCode = tradePair.code
        wiccTradeDetail.tradePrice = keyInfo.tradePrice
        wiccTradeDetail.tradeMoney = keyInfo.tradeMoney
        wiccTradeDetail.tradedAtMs = System.currentTimeMillis()
        wiccTradeDetail.status = TradeDetailStatus.MATCHED.code

        return wiccTradeDetail
    }


    private fun provideTradeAmount(price: BigDecimal, sellRequest: WiccTradeRequest, buyRequest: WiccTradeRequest): BigDecimal{
        return min( sellRequest.remainTradeAmount(), buyRequest.remainTradeAmount(price) )
    }

    private fun prodiveTradePrice(sellRequest: WiccTradeRequest, buyRequest: WiccTradeRequest): BigDecimal?{

        if(sellRequest.isLimitPriceRequest() && buyRequest.isLimitPriceRequest()) {
            if( sellRequest.targetPrice > buyRequest.targetPrice)
                return null
            return sellRequest.targetPrice
        }

        if(sellRequest.isLimitPriceRequest())
            return sellRequest.targetPrice

        if (buyRequest.isLimitPriceRequest())
            return buyRequest.targetPrice

        return determineAllMarketRequestPrice()
    }

    private fun determineAllMarketRequestPrice(): BigDecimal?{

        var maxBuyPrice = buyRequestList!!.findMaxPossiblePrice()?: return null
        var minSellPrice = sellRequestList!!.findMaxPossiblePrice()?: return null
        return (maxBuyPrice.add(minSellPrice)).divide(2.toBigDecimal(), GLOBAL_DECIMAL_DIGITS, BigDecimal.ROUND_HALF_UP)
    }

    private fun detimineTradeDetailKeyInfo(sellRequest: WiccTradeRequest, buyRequest: WiccTradeRequest): TradeDetailKeyInfo?{
        val tradePrice  = prodiveTradePrice(sellRequest, buyRequest)?: return null
        val tradeAmount = provideTradeAmount(tradePrice, sellRequest, buyRequest)
        val tradeMoney  = provideTradeMoney(tradePrice,tradeAmount, buyRequest)
        return TradeDetailKeyInfo(tradePrice,tradeAmount,tradeMoney)
    }

    private fun provideTradeMoney(price: BigDecimal, tradeAmount: BigDecimal,buyRequest: WiccTradeRequest): BigDecimal{

        if(!buyRequest.isBuyRequest()){
            throw IllegalArgumentException("you must provide a WiccTradeRequest that its match direction is 'buy'")
        }

        val tradeMoney =  if(tradeAmount == buyRequest.remainTradeAmount(price) && buyRequest.isMarketPriceRequest()){
            buyRequest.remainDealMoney()
        }else {
            var money = price.multiply(tradeAmount).setScale(GLOBAL_DECIMAL_DIGITS , BigDecimal.ROUND_DOWN)
            if(money <= BigDecimal.ZERO)
                money =  GLOBAL_MINIMUM_TRADE_UNIT

            money
        }
        return tradeMoney
    }


    @Volatile var running = true
    private var sellRequestList: PendingRequestList? = null
    private var buyRequestList: PendingRequestList? = null
    private var wiccTradeDetailList = arrayListOf<WiccTradeDetail>()
    private var totalMatchedTradeAmount = BigDecimal.ZERO
    private var totalMatchedMoneyAmount = BigDecimal.ZERO
    private var tradePair: WiccTradePair = WiccTradePair()
    private var settleTxHash: String? = null

    private val logger = LoggerFactory.getLogger(javaClass)

    private val wiccTradeService               = SpringContextAccessor.getBean(WiccTradeService::class.java)
    private val wiccTradeRequestService  = SpringContextAccessor.getBean(WiccTradeRequestService::class.java)
    private val wiccTradeDetailService     = SpringContextAccessor.getBean(WiccTradeDetailService::class.java)
    private val redisLockService                = SpringContextAccessor.getBean(RedisLockService::class.java)
    private val coinHandler                         = SpringContextAccessor.getBean(CoinHandler::class.java)

    private class TradeDetailKeyInfo(var tradePrice: BigDecimal,var tradeAmount: BigDecimal, var tradeMoney: BigDecimal)



}