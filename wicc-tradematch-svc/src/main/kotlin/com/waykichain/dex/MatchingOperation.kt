package com.waykichain.dex

import com.waykichain.dex.entity.domain.WiccTradePair

/**
 *  Created by yehuan on 2019/6/17
 */

interface MatchingOperation{

    fun match()

    fun tradePair(): WiccTradePair

    fun stopMatch()

}