package com.waykichain.utils;

import com.waykichain.commons.util.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by joslin on 2017/6/30.
 */
public class BizDateUtils {

  static public Date getBeginAssignDate() {
    DateTime dateTime = new DateTime();
    if (dateTime.getDayOfMonth() < 16) {
      return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(),
          1, 0, 0, 0).toDate();
    } else {
      return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(),
          16, 0, 0, 0).toDate();
    }
  }

  /**
   * Get date from milli
   *
   * @param milli 毫秒值
   */
  static public Date getDate(Long milli) {
    return Date.from(Instant.ofEpochMilli(milli));
  }

  /**
   * 获取当前时间 减少天数的0点
   */

  static public Date minusDayStart(Integer days) {
    DateTime dt = new DateTime();
    dt = dt.minusDays(days).withTimeAtStartOfDay();
    return dt.toDate();
  }

  static public Date plusSecounds(Integer secounds) {
    DateTime dt = new DateTime();
    dt = dt.plusSeconds(secounds);
    return dt.toDate();
  }

  /**
   * 获取当前时间 减少天数日期的23.59.59点
   */

  static public Date minusDayEnd(Integer days) {
    if (days == 0) {
      return new Date();
    }

    DateTime dt = new DateTime();
    dt = dt.minusDays(days - 1).withTimeAtStartOfDay().minusSeconds(1);
    return dt.toDate();
  }


  static public Date plusDayEnd(Integer days) {
    DateTime dt = new DateTime();
    dt = dt.plusDays(days + 1).withTimeAtStartOfDay().minusSeconds(1);
    return dt.toDate();
  }

  static public Date plusDayEnd(Date date, Integer days) {
    DateTime dt = new DateTime(date);
    dt = dt.plusDays(days + 1).withTimeAtStartOfDay().minusSeconds(1);
    return dt.toDate();
  }

  static public Long getMinStart(Long milliSecond) {
    DateTime dt = new DateTime(milliSecond);
    dt = dt.minusSeconds(dt.getSecondOfMinute());
    dt = dt.minusMillis(dt.getMillisOfSecond());
    return dt.getMillis();
  }

  /**
   * 根据日期获取逾期天数
   */
  static public int getOverdueDay(Date dueTime) {
    DateTime now = new DateTime();
    now = now.withTimeAtStartOfDay();
    DateTime dueBegin = new DateTime(dueTime);
    dueBegin = dueBegin.withTimeAtStartOfDay();
    int overdueDays = Days.daysBetween(dueBegin, now).getDays();
    if (overdueDays < 0) {
      overdueDays = 0;
    }
    return overdueDays;
  }

  /**
   * 还款天数
   */
  static public int getRepayDays(Date dueTime) {
    DateTime now = new DateTime();
    now = now.withTimeAtStartOfDay();
    DateTime dueBegin = new DateTime(dueTime);
    dueBegin = dueBegin.withTimeAtStartOfDay();
    int repayDays = Days.daysBetween(now, dueBegin).getDays();
    if (repayDays < 0) {
      repayDays = 0;
    }
    return repayDays;
  }

  static public int getOverdueDay(Date dueTime, Date finishTime) {
    DateTime finishDatetime = new DateTime(finishTime);
    finishDatetime = finishDatetime.withTimeAtStartOfDay();
    DateTime dueBegin = new DateTime(dueTime);
    dueBegin = dueBegin.withTimeAtStartOfDay();
    return Days.daysBetween(dueBegin, finishDatetime).getDays();
  }


  /**
   * 根据逾期日期获取进案日期
   */
  static public Date getCaseInTime(Date dueTime) {
    DateTime dt = new DateTime(dueTime);
    dt = dt.plusDays(1);
    return dt.toDate();
  }

  /**
   * 根据生日获取年龄
   */
  static public int getAge(Date bob) {
    DateTime now = new DateTime();
    DateTime dueBegin = new DateTime(bob);
    return now.getYear() - dueBegin.getYear() + 1;

  }

  /**
   * 获取日期的0点
   */
  static public Date getBegOfDay(Date day) {
    DateTime dt = new DateTime(day);
    return dt.withTimeAtStartOfDay().toDate();
  }

  /**
   * 获取日期的23.59.59s
   */
  static public Date getEndOfDay(Date day) {
    DateTime dt = new DateTime(day);
    dt = dt.plusDays(1).withTimeAtStartOfDay().minusSeconds(1);
    return dt.toDate();
  }


  /**
   * 获得指定日期的相隔多少天的日期
   */
  public static Date getSpecifiedDay(Date date, int interval) {
    DateTime dt = new DateTime(date);
    dt = dt.plusDays(interval);
    return dt.toDate();
  }

  public static Long getDiffSeconds(Date dateFrom, Date dateTo) {
    long seconds = (dateFrom.getTime() - dateFrom.getTime()) / 1000;
    return seconds;
  }

  public static Date beforeOneHourStart() {

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static Date beforeOneHourStart(Date start) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(start);
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static Date beforeOneHourEnd() {

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static Date beforeOneHourEnd(Date end) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(end);
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static int getHour(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(Calendar.HOUR_OF_DAY);
  }

  public static int getDay(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(Calendar.DAY_OF_YEAR);
  }

  public static Date getYestordayBegin(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static Date getYestordayEnd(Date date) {

    Calendar calendar = Calendar.getInstance();

    calendar.setTime(date);
    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  //获取指定日期当天开始
  public static Date getDateBegin(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  //获取指定日期当天结束
  public static Date getDateEnd(Date date) {

    Calendar calendar = Calendar.getInstance();

    calendar.setTime(date);
    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static  Date parseDateStrToDefaultZone( String dateStr, TimeZone zone) {

    Date date = DateUtils.getDate(dateStr, DateUtils.DEFAULTDATEPATTERN);

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date) ;
    calendar.set(Calendar.MILLISECOND, zone.getRawOffset());
    calendar.setTimeZone(TimeZone.getDefault());
    return calendar.getTime();
  }

  public static TimeZone parseTimezone(String timeZone)  {

    String zone = timeZone;
    TimeZone  defaultTimeZone = TimeZone.getDefault();
    TimeZone requestTimeZone = TimeZone.getDefault();

      try {
        int hour = Integer.parseInt(zone);
        if (hour >= 0) {
          zone = "GMT+"+hour;
        } else {
          zone = "GMT"+hour;
        }

        requestTimeZone = StringUtils.parseTimeZoneString(zone);
      } catch (Exception e ) {
        e.printStackTrace();
    }

    int offsetHours = (defaultTimeZone.getRawOffset() - requestTimeZone.getRawOffset()) / (60 * 60 * 1000);
      if (offsetHours >= 0) {
        zone = "GMT+"+offsetHours;
      } else {
        zone = "GMT"+offsetHours;
      }

      return StringUtils.parseTimeZoneString(zone);
  }

}
