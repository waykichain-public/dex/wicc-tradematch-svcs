package com.waykichain.task.beans

/**
 * Created by yinjun
 * @date 2018/4/2
 */

data class DataTablePageVO<out T>(val list: List<T>? = null,
                                  val pageIndex: Int? = null,
                                  val pageSize: Int? = null,
                                  val count: Long,
                                  val amount:Long? = null)