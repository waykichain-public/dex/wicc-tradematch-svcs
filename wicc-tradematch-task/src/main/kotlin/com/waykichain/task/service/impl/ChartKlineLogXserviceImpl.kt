package com.waykichain.task.service.impl

import com.querydsl.core.types.dsl.BooleanExpression
import com.waykichain.dex.commons.biz.service.ChartKlineLogService
import com.waykichain.dex.entity.domain.ChartKlineLog
import com.waykichain.dex.entity.domain.QChartKlineLog
import com.waykichain.task.beans.ChartKlineLogPO
import com.waykichain.task.beans.ChartKlineLogVO
import com.waykichain.task.beans.DataTablePageVO
import com.waykichain.task.beans.GenKlineCond
import com.waykichain.dex.base.dict.PeriodType
import com.waykichain.dex.base.sumByBigDecimal
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.task.service.ChartKlineLogXservice
import com.waykichain.utils.BizDateUtils
import org.apache.commons.beanutils.BeanUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import javax.transaction.Transactional

@Service
open class ChartKlineLogXserviceImpl : ChartKlineLogXservice {

    override fun list(po: ChartKlineLogPO): DataTablePageVO<ChartKlineLogVO> {
        logger.debug("symbol ${po.symbol},period_code ${po.periodCode}")
        val predicate = createPredicate(po)
        val pr = PageRequest(po.pageIndex!! - 1, po.pageSize!!)
        val result = chartKlineLogService.find(predicate, pr)
        logger.info("getKline pageSize:${result.content.size}")
        return DataTablePageVO(
                list = result.content.map { convert2VO(it) },
                count = result.totalElements)
    }

    override fun genMinKline(tradePairCode: String, trades: List<WiccTradeDetail>): ChartKlineLog? {
        logger.debug("generate k-line of $tradePairCode")
        val pairedTrades = trades.filter { it.tradePairCode == tradePairCode }

        val kline = ChartKlineLog()
        kline.symbol = tradePairCode
        kline.periodCode = PeriodType.ONE_MIN.code
        if (pairedTrades.isEmpty()) {
            val latest = chartKlineLogService.getLatestEos(tradePairCode, PeriodType.ONE_MIN.code)
            if (latest == null) {
                logger.error("not exits k-line history")
                return null
            }
            kline.tradeDetailCount = 0
            kline.tradeAmount = BigDecimal.ZERO
            kline.tradeMoney = BigDecimal.ZERO
            kline.openPrice = latest.closePrice
            kline.closePrice = latest.closePrice
            kline.highPrice = latest.closePrice
            kline.lowPrice = latest.closePrice
            kline.eos = BizDateUtils.getMinStart(latest.eos.plus(PeriodType.ONE_MIN.sec.times(1000)))
            kline.eosDate = Date.from(Instant.ofEpochMilli(kline.eos))
        } else {
            kline.tradeDetailCount = pairedTrades.size
            kline.tradeAmount = pairedTrades.sumByBigDecimal { it.tradeAmount }
            kline.tradeMoney = pairedTrades.sumByBigDecimal { it.tradeMoney }

            val priceSorted = pairedTrades.sortedBy { it.tradePrice }
            kline.lowPrice = priceSorted.first().tradePrice
            kline.highPrice = priceSorted.last().tradePrice

            val eomsSorted = pairedTrades.sortedBy { it.tradedAtMs }
            kline.openPrice = eomsSorted.first().tradePrice
            kline.closePrice = eomsSorted.last().tradePrice

            kline.eos = BizDateUtils.getMinStart(eomsSorted.first().tradedAtMs)
            kline.eosDate = Date.from(Instant.ofEpochMilli(kline.eos))
        }
        return chartKlineLogService.save(kline)
    }

    @Transactional
    override fun genNonMinKline(cond: GenKlineCond): ChartKlineLog? {
        // get 1min k-lines
        val lines = chartKlineLogService.getKlineList(cond.begEos, cond.endEos, PeriodType.ONE_MIN.code,cond.symbol)

        val kline = ChartKlineLog()
        // common
        kline.symbol = cond.symbol
        kline.periodCode = cond.periodCode

        if (lines.isEmpty()) {
            val latest = chartKlineLogService.getLatestEos(cond.symbol, cond.periodCode)
            if (latest == null) {
                logger.error("not exits k-line history")
                return null
            }
            kline.tradeDetailCount = 0
            kline.tradeAmount = BigDecimal.ZERO
            kline.tradeMoney = BigDecimal.ZERO
            kline.openPrice = latest.closePrice
            kline.closePrice = latest.closePrice
            kline.highPrice = latest.closePrice
            kline.lowPrice = latest.closePrice
            kline.eos = BizDateUtils.getMinStart(latest.eos.plus(PeriodType.ONE_MIN.sec.times(1000)))
            kline.eosDate = Date.from(Instant.ofEpochMilli(kline.eos))
            return chartKlineLogService.save(kline)
        }// end

        // date
        val eosSorted = lines.sortedBy { it.eos } // asc: natural sort
        kline.eos = cond.begEos // must before eosDate
        kline.eosDate = Date.from(Instant.ofEpochMilli(kline.eos))
        kline.openPrice = eosSorted.first().openPrice
        kline.closePrice = eosSorted.last().closePrice

        // stats
        kline.highPrice = lines.sortedBy { it.highPrice }.last().highPrice
        kline.lowPrice = lines.sortedBy { it.lowPrice }.first().lowPrice

        // summary
        kline.tradeAmount = lines.sumByBigDecimal { it.tradeAmount }
        kline.tradeDetailCount = lines.sumBy { it.tradeDetailCount }
        kline.tradeMoney = lines.sumByBigDecimal { it.tradeMoney }

        return chartKlineLogService.save(kline)
    }

    private fun convert2VO(chartKlineLog: ChartKlineLog): ChartKlineLogVO {
        val vo = ChartKlineLogVO()
        BeanUtils.copyProperties(vo, chartKlineLog)
        return vo
    }

    private fun createPredicate(po: ChartKlineLogPO): BooleanExpression {
        return QChartKlineLog.chartKlineLog.symbol.eq(po.symbol)
                .and(QChartKlineLog.chartKlineLog.eosDate.between(
                        BizDateUtils.getDate(po.eosDateBeg!!),
                        BizDateUtils.getDate(po.eosDateEnd!!)))
                .and(QChartKlineLog.chartKlineLog.periodCode.eq(po.periodCode))
    }

    private val logger = LoggerFactory.getLogger(javaClass)
    @Autowired
    private lateinit var chartKlineLogService: ChartKlineLogService
}