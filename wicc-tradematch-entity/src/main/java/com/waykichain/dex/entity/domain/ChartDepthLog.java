package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * ChartDepthLog is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class ChartDepthLog implements Serializable {

    @Column("count")
    private java.math.BigDecimal count;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("depth")
    private String depth;

    @Column("eoms")
    private Long eoms;

    @Column("eos_date")
    private java.util.Date eosDate;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("price")
    private java.math.BigDecimal price;

    @Column("side_code")
    private String sideCode;

    @Column("trade_pair_code")
    private String tradePairCode;

    @Column("updated_at")
    private java.util.Date updatedAt;

    @Column("vol_amount")
    private java.math.BigDecimal volAmount;

    public java.math.BigDecimal getCount() {
        return count;
    }

    public void setCount(java.math.BigDecimal count) {
        this.count = count;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public Long getEoms() {
        return eoms;
    }

    public void setEoms(Long eoms) {
        this.eoms = eoms;
    }

    public java.util.Date getEosDate() {
        return eosDate;
    }

    public void setEosDate(java.util.Date eosDate) {
        this.eosDate = eosDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getPrice() {
        return price;
    }

    public void setPrice(java.math.BigDecimal price) {
        this.price = price;
    }

    public String getSideCode() {
        return sideCode;
    }

    public void setSideCode(String sideCode) {
        this.sideCode = sideCode;
    }

    public String getTradePairCode() {
        return tradePairCode;
    }

    public void setTradePairCode(String tradePairCode) {
        this.tradePairCode = tradePairCode;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public java.math.BigDecimal getVolAmount() {
        return volAmount;
    }

    public void setVolAmount(java.math.BigDecimal volAmount) {
        this.volAmount = volAmount;
    }

}

