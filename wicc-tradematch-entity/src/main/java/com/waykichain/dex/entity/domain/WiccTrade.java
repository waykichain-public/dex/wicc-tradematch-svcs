package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccTrade is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccTrade implements Serializable {

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("deal_coin_symbol")
    private String dealCoinSymbol;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("market_coin_symbol")
    private String marketCoinSymbol;

    @Column("status")
    private Integer status;

    @Column("trade_amount")
    private java.math.BigDecimal tradeAmount;

    @Column("trade_detail_count")
    private Integer tradeDetailCount;

    @Column("trade_money")
    private java.math.BigDecimal tradeMoney;

    @Column("trade_pair_code")
    private String tradePairCode;

    @Column("tx_hash")
    private String txHash;

    @Column("tx_height")
    private Integer txHeight;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDealCoinSymbol() {
        return dealCoinSymbol;
    }

    public void setDealCoinSymbol(String dealCoinSymbol) {
        this.dealCoinSymbol = dealCoinSymbol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarketCoinSymbol() {
        return marketCoinSymbol;
    }

    public void setMarketCoinSymbol(String marketCoinSymbol) {
        this.marketCoinSymbol = marketCoinSymbol;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.math.BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(java.math.BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getTradeDetailCount() {
        return tradeDetailCount;
    }

    public void setTradeDetailCount(Integer tradeDetailCount) {
        this.tradeDetailCount = tradeDetailCount;
    }

    public java.math.BigDecimal getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(java.math.BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    public String getTradePairCode() {
        return tradePairCode;
    }

    public void setTradePairCode(String tradePairCode) {
        this.tradePairCode = tradePairCode;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public Integer getTxHeight() {
        return txHeight;
    }

    public void setTxHeight(Integer txHeight) {
        this.txHeight = txHeight;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

