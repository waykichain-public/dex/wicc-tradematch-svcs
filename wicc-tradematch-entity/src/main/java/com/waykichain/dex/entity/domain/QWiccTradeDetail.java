package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccTradeDetail is a Querydsl query type for WiccTradeDetail
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccTradeDetail extends com.querydsl.sql.RelationalPathBase<WiccTradeDetail> {

    private static final long serialVersionUID = 1117300864;

    public static final QWiccTradeDetail wiccTradeDetail = new QWiccTradeDetail("wicc_trade_detail");

    public final StringPath buyOrderTxHash = createString("buyOrderTxHash");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath sellOrderTxHash = createString("sellOrderTxHash");

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> tradeAmount = createNumber("tradeAmount", java.math.BigDecimal.class);

    public final NumberPath<Long> tradedAtMs = createNumber("tradedAtMs", Long.class);

    public final NumberPath<java.math.BigDecimal> tradeMoney = createNumber("tradeMoney", java.math.BigDecimal.class);

    public final NumberPath<Long> tradeOrderId = createNumber("tradeOrderId", Long.class);

    public final StringPath tradePairCode = createString("tradePairCode");

    public final NumberPath<java.math.BigDecimal> tradePrice = createNumber("tradePrice", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<WiccTradeDetail> primary = createPrimaryKey(id);

    public QWiccTradeDetail(String variable) {
        super(WiccTradeDetail.class, forVariable(variable), "null", "wicc_trade_detail");
        addMetadata();
    }

    public QWiccTradeDetail(String variable, String schema, String table) {
        super(WiccTradeDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccTradeDetail(String variable, String schema) {
        super(WiccTradeDetail.class, forVariable(variable), schema, "wicc_trade_detail");
        addMetadata();
    }

    public QWiccTradeDetail(Path<? extends WiccTradeDetail> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_trade_detail");
        addMetadata();
    }

    public QWiccTradeDetail(PathMetadata metadata) {
        super(WiccTradeDetail.class, metadata, "null", "wicc_trade_detail");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(buyOrderTxHash, ColumnMetadata.named("buy_order_tx_hash").withIndex(5).ofType(Types.VARCHAR).withSize(128));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(11).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(sellOrderTxHash, ColumnMetadata.named("sell_order_tx_hash").withIndex(4).ofType(Types.VARCHAR).withSize(128));
        addMetadata(status, ColumnMetadata.named("status").withIndex(9).ofType(Types.INTEGER).withSize(10));
        addMetadata(tradeAmount, ColumnMetadata.named("trade_amount").withIndex(6).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(tradedAtMs, ColumnMetadata.named("traded_at_ms").withIndex(10).ofType(Types.BIGINT).withSize(19));
        addMetadata(tradeMoney, ColumnMetadata.named("trade_money").withIndex(8).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(tradeOrderId, ColumnMetadata.named("trade_order_id").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(tradePairCode, ColumnMetadata.named("trade_pair_code").withIndex(3).ofType(Types.VARCHAR).withSize(64));
        addMetadata(tradePrice, ColumnMetadata.named("trade_price").withIndex(7).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(12).ofType(Types.TIMESTAMP).withSize(19));
    }

}

