package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QChartKlineLog is a Querydsl query type for ChartKlineLog
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QChartKlineLog extends com.querydsl.sql.RelationalPathBase<ChartKlineLog> {

    private static final long serialVersionUID = 1889675488;

    public static final QChartKlineLog chartKlineLog = new QChartKlineLog("chart_kline_log");

    public final NumberPath<java.math.BigDecimal> closePrice = createNumber("closePrice", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> eos = createNumber("eos", Long.class);

    public final DateTimePath<java.util.Date> eosDate = createDateTime("eosDate", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> highPrice = createNumber("highPrice", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> lowPrice = createNumber("lowPrice", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> openPrice = createNumber("openPrice", java.math.BigDecimal.class);

    public final StringPath periodCode = createString("periodCode");

    public final StringPath symbol = createString("symbol");

    public final NumberPath<java.math.BigDecimal> tradeAmount = createNumber("tradeAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> tradeDetailCount = createNumber("tradeDetailCount", Integer.class);

    public final NumberPath<java.math.BigDecimal> tradeMoney = createNumber("tradeMoney", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<ChartKlineLog> primary = createPrimaryKey(id);

    public QChartKlineLog(String variable) {
        super(ChartKlineLog.class, forVariable(variable), "null", "chart_kline_log");
        addMetadata();
    }

    public QChartKlineLog(String variable, String schema, String table) {
        super(ChartKlineLog.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QChartKlineLog(String variable, String schema) {
        super(ChartKlineLog.class, forVariable(variable), schema, "chart_kline_log");
        addMetadata();
    }

    public QChartKlineLog(Path<? extends ChartKlineLog> path) {
        super(path.getType(), path.getMetadata(), "null", "chart_kline_log");
        addMetadata();
    }

    public QChartKlineLog(PathMetadata metadata) {
        super(ChartKlineLog.class, metadata, "null", "chart_kline_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(closePrice, ColumnMetadata.named("close_price").withIndex(10).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(13).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(eos, ColumnMetadata.named("eos").withIndex(4).ofType(Types.BIGINT).withSize(19));
        addMetadata(eosDate, ColumnMetadata.named("eos_date").withIndex(5).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(highPrice, ColumnMetadata.named("high_price").withIndex(12).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(lowPrice, ColumnMetadata.named("low_price").withIndex(11).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(openPrice, ColumnMetadata.named("open_price").withIndex(9).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(periodCode, ColumnMetadata.named("period_code").withIndex(3).ofType(Types.VARCHAR).withSize(32));
        addMetadata(symbol, ColumnMetadata.named("symbol").withIndex(2).ofType(Types.VARCHAR).withSize(32));
        addMetadata(tradeAmount, ColumnMetadata.named("trade_amount").withIndex(6).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(tradeDetailCount, ColumnMetadata.named("trade_detail_count").withIndex(8).ofType(Types.INTEGER).withSize(10));
        addMetadata(tradeMoney, ColumnMetadata.named("trade_money").withIndex(7).ofType(Types.DECIMAL).withSize(36).withDigits(8));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(14).ofType(Types.TIMESTAMP).withSize(19));
    }

}

