package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * ChartKlineLog is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class ChartKlineLog implements Serializable {

    @Column("close_price")
    private java.math.BigDecimal closePrice;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("eos")
    private Long eos;

    @Column("eos_date")
    private java.util.Date eosDate;

    @Column("high_price")
    private java.math.BigDecimal highPrice;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("low_price")
    private java.math.BigDecimal lowPrice;

    @Column("open_price")
    private java.math.BigDecimal openPrice;

    @Column("period_code")
    private String periodCode;

    @Column("symbol")
    private String symbol;

    @Column("trade_amount")
    private java.math.BigDecimal tradeAmount;

    @Column("trade_detail_count")
    private Integer tradeDetailCount;

    @Column("trade_money")
    private java.math.BigDecimal tradeMoney;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public java.math.BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(java.math.BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getEos() {
        return eos;
    }

    public void setEos(Long eos) {
        this.eos = eos;
    }

    public java.util.Date getEosDate() {
        return eosDate;
    }

    public void setEosDate(java.util.Date eosDate) {
        this.eosDate = eosDate;
    }

    public java.math.BigDecimal getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(java.math.BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(java.math.BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    public java.math.BigDecimal getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(java.math.BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public String getPeriodCode() {
        return periodCode;
    }

    public void setPeriodCode(String periodCode) {
        this.periodCode = periodCode;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public java.math.BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(java.math.BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getTradeDetailCount() {
        return tradeDetailCount;
    }

    public void setTradeDetailCount(Integer tradeDetailCount) {
        this.tradeDetailCount = tradeDetailCount;
    }

    public java.math.BigDecimal getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(java.math.BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

