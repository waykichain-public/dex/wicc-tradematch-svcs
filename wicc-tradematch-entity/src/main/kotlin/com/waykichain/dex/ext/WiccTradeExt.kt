package com.waykichain.dex.ext

import com.waykichain.dex.base.dict.TradeStatus
import com.waykichain.dex.entity.domain.WiccTrade

/**
 *  Created by yehuan on 2019/6/22
 */

fun WiccTrade.needRollback(): Boolean{

    return  TradeStatus.needRollback(status)
}
