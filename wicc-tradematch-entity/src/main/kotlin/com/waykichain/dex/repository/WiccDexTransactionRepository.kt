package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccDexTransaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccDexTransactionRepository : JpaRepository<WiccDexTransaction, Long>,
    QueryDslPredicateExecutor<WiccDexTransaction>
