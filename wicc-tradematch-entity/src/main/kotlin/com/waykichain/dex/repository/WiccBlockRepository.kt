package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccBlock
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccBlockRepository : JpaRepository<WiccBlock, Long>,
    QueryDslPredicateExecutor<WiccBlock>
